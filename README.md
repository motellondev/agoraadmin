
1. Install git, python and pip.

2. Clone the repository with:

git clone https://gitlab.com/Seburath/agoraadmin/

3. Go into the directory with:

cd agoraadmin

4. Install the dependencies: 

pip install -r requirements.txt

5. Install the package with: 

pip install -e . 

6. Run the bot with:

python3 -m agoraadmin
