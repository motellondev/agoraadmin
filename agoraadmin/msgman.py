#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .telman import TelMan


class MsgMan(TelMan):
    """Messages manager."""

    def process_text(self, text):
        pass

    def process_button(self, button):
        pass
