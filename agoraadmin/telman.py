#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
import yaml

from telegram.ext import Updater


class TelMan:
    """Telegram Manager."""

    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        level=logging.INFO,
    )
    log = logging.getLogger(__name__).info
    separator = "---------------------------------\n"

    def __init__(self):
        self.config = self.read_config()
        self.log(self.config)

        self.updater = Updater(self.config["TELEGRAM_TOKEN"], use_context=True)

        self.identity = ""
        self.user = ""
        self.status = ""

        import pickle

        try:
            foo = pickle.load(open("data.pickle", "rb"))
        except (OSError, IOError) as e:
            foo = []
            pickle.dump(foo, open("data.pickle", "wb"))

    def read_config(self):
        with open("config.yml", "r") as f:
            return yaml.safe_load(f)

    def update_updater(self):
        self.updater = Updater(self.config["TELEGRAM_TOKEN"], use_context=True)

    def in_chat(self):
        chat = self.update.effective_chat
        return chat["id"] == int(self.config["CHAT_ID"])

    def set_update(self, update):
        self.update = update

    def set_query(self, update):
        self.query = update.callback_query

    def set_status(self, status):
        self.log(status)
        self.status = status

    def reply(self, msg, keyboard=None):
        reply = self.update.message.reply_text
        reply(msg, reply_markup=keyboard)

    def edit(self, msg, keyboard=None):
        edit = self.query.edit_message_text
        edit(msg, reply_markup=keyboard)

    def send(self, msg, keyboard=None):
        send = self.updater.bot.send_message
        send(self.chat_id, msg, reply_markup=keyboard)

    def erase_msg(self, msg_id):
        erase = self.updater.bot.delete_message
        erase(self.update.message.chat_id, msg_id)

    def erase_updated_message(self):
        self.erase_msg(self.update.message.message_id)

    def erase_last_updated_message(self):
        self.erase_msg(self.update.message.message_id - 1)

    def get_admins(self, update):
        get_admins = update.effective_chat.get_administrators
        admins = [admin.user.username for admin in get_admins(100000)]

        return admins

    def get_user(self):
        if self.user == "":
            self.user = self.update.message.from_user.username

        return self.user

    def is_admin_call(self, update):
        return self.get_user(update) in self.get_admins(update)

    def is_admin_in_chat(self, update):
        return self.is_admin_call(update) and self.in_chat()
